# 6 expert

## build

```bash
docker build -t bind-mount-beispiel .
```

## run

```bash
docker run -d -p 8080:80 -v $(pwd):/var/www/html bind-mount-beispiel
```

Das file kann nun geändert werden. Bei reload sollten die Änderungen angezeit werden.